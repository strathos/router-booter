#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

urls=(
    https://www.google.fi
    https://yle.fi
)

OUTPIN="16"
LOGFILENAME="router-booter.log"
MAXFILESIZE=10000000

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [ -d ${DIR}/logs ]; then
    if [ -f ${DIR}/logs/${LOGFILENAME} ]; then
        file_size=$(du -b ${DIR}/logs/${LOGFILENAME} | tr -s '\t' ' ' | cut -d' ' -f1)
        if [ ${file_size} -gt ${MAXFILESIZE} ]; then
            savelog -l -n -c 10 ${DIR}/logs/${LOGFILENAME} >/dev/null 2>&1
        fi
    fi
else
    mkdir ${DIR}/logs
fi


PINPATH=/sys/class/gpio/gpio${OUTPIN}
echo ${OUTPIN} > /sys/class/gpio/export 2>/dev/null || true
echo "out" > ${PINPATH}/direction
echo "0" > ${PINPATH}/value

status=0

for url in "${urls[@]}"; do
    if curl -s -I -L --fail ${url} >/dev/null; then
        status=1
    fi
done

echo "$(date --iso-8601=seconds) Status: ${status}" >> ${DIR}/logs/${LOGFILENAME}

if [ ${status} -eq 0 ]; then
    echo "1" > ${PINPATH}/value
    sleep 5
    echo "0" > ${PINPATH}/value
    echo "$(date --iso-8601=seconds) Router restarted" >> ${DIR}/logs/${LOGFILENAME}
    exit 0
else
    exit 0
fi

